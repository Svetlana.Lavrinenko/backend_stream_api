import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Task11 {


    public static void main(String[] args) throws IOException {

        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));

        List<String> list=new ArrayList<>();
        String line=null;
        while ((line=reader.readLine())!=null) {
            list.addAll(List.of(line.toLowerCase().replaceAll("\\p{Punct}", "").split(" ")));

        }

        HashMap<String, Integer> map = new HashMap<>();

        list.stream().forEach(word ->
        {
            if (map.containsKey(word)) map.put(word, map.get(word) + 1);
            else map.put(word, 1);
        });

        map.entrySet().stream()
                .sorted((w1, w2) -> {
                    if(w1.getValue() == w2.getValue()) return w1.getKey().compareTo(w2.getKey());
                    else return w2.getValue().compareTo(w1.getValue());})
                .limit(10)
                .forEach(e -> System.out.println(e.getKey()));

    }
}
